#! /bin/bash -u

function usage()
{
  echo "gl2gh.sh <github dir>"
}


# Check options
if [ $# -ne 1 ]
then
  echo "Syntax error !"
  usage
  exit 1
fi

if [ $1 == '--help' -o "$1" == '-h' ]
then
  usage
  exit 0
fi

GITHUB_DIR="$1"

if [ ! -d "$GITHUB_DIR" ]
then
  echo "$GITHUB_DIR not found or not readable"
  exit 2
fi

# Process Hacker News post
function process_hn_post()
{
  file="$1"
  filename="$2"
  fr_filename="$3"
  echo "Processing Hacker News post $filename"
  cp "$file" ./content/hackernews/"$fr_filename"
  echo
}

# Process non HN post
function process_other_post()
{
  file="$1"
  filename="$2"
  echo "Processing other post $filename"
  cp "$file" ./content/post/"$fr_filename"
  echo
}

post_files=$(find "$GITHUB_DIR/_posts/" -name '*.md' )

for post in ${post_files[@]}
do
  #echo $post
  filename=$(basename $post)
  #echo $filename
  if [[ ! "$filename" =~ (^20[0-9]{2}-[0-1][0-9]-[0-3][0-9]-) ]]
  then
    continue
  fi
  echo $filename
  fr_filename=${filename%.md}.fr.md
  if [[ "$filename" =~ hackernews ]]
  then
    process_hn_post "$post" "$filename" "$fr_filename"
  else
    process_other_post "$post" "$filename" "$fr_filename"
  fi
done
